import { Component, OnInit } from '@angular/core';
import { AlertPage } from '../alert/alert.page';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

componentes: Componente [] = [
  {
    icon: 'apps',
    name: 'action-sheet',
    redirectTo: '/action-sheet'
  },
  {
    icon: 'alert',
    name: 'Alert',
    redirectTo: '/alert'
  },
  {
    icon: 'alarm',
    name: 'Alarm',
    redirectTo: '/'
  },
]
  constructor() { }
  ngOnInit() {
  }

}

interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}
